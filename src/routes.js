const express = require('express');
const TagsController = require('./app/controllers/TagsController');
const TaskController = require('./app/controllers/TaskController');
const TasklistController = require('./app/controllers/TasklistController');

const routes = new express.Router();

routes.post('/tags', TagsController.grava);
routes.get('/tags', TagsController.consulta);
routes.patch('/tags:id', TagsController.atualiza);
routes.delete('/tags:id', TagsController.remove);

routes.post('/tasks', TaskController.grava);
routes.get('/tasks', TaskController.consulta);
routes.patch('/tasks:id', TaskController.atualiza);
routes.delete('/tasks:id', TaskController.remove);

routes.post('/tasklist', TasklistController.grava);
routes.get('/tasklist', TasklistController.consulta);
routes.patch('/tasklist:id', TasklistController.atualiza);
routes.delete('/tasklist:id', TasklistController.remove);

module.exports = routes;
