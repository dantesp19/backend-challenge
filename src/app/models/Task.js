const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema(
  {
    id: {
      type: String,
      required: true,
      unique: true,
    },
    name: {
      type: String,
      required: true,
    },
    notes: {
      type: String,
      required: true,
    },
    priority: {
      type: String,
      required: true,
    },
    remindMeOn: {
      type: Date,
      required: true,
    },
    activityType: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    tasklist: {
      type: String,
      required: true,
    },
    tags: {
      type: Array,
      required: true
    }
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('tasks', TaskSchema);
