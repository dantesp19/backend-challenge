const Task = require('../models/Task');
const { v4: uuidv4 } = require('uuid');

class TaskController {

  async grava(req, res) {
    try {
      const task = new Task({
        id: uuidv4(),
        name: req.body.name,
        notes:req.body.notes,
        priority: req.body.priority,
        remindMeOn: req.body.remindMeOn,
        activityType: req.body.activityType,
        status: req.body.status,
        tasklist: req.body.tasklist,
        tags: req.body.tags,
      });
      await task.save();
      return res.json(task);
    } catch (err) {
      return res.status(500).json({ error: 'Internal server error' });
    }
  }

  async consulta(req, res) {
    try {
      const task = await Task.find({});
      if (task) {
        await task;
      }
      return res.send();
    } catch (err) {
      return res.status(400).json({ error: 'Task not found' });
    }
  }

  async atualiza(req, res) {
    try {
      const task = await Task.findById(req.body.id);
      if (task) {
        await task.updateOne();
      }
      return res.send();
    } catch (err) {
      return res.status(400).json({ error: 'Task not found' });
    }
  }

  async remove(req, res) {
    try {
      const task = await Task.findById(req.body.id);
      if (task) {
        await task.remove();
      }
      return res.send();
    } catch (err) {
      return res.status(400).json({ error: 'Task not found' });
    }
  }

}

module.exports = new TaskController();
