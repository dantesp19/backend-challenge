const Tags = require('../models/Tags');
const { v4: uuidv4 } = require('uuid');

class TagsController {

  async grava(req, res) {
    try {
      let i = 0;
      const tags = new Tags({
        id: uuidv4(),
        name: req.body.name,
        count: i++,
      });
      await tags.save();
      return res.json({
        id: tags.id,
        name: tags.name,
        count: tags.count,
      });
    } catch (err) {
      return res.status(500).json({ error: 'Internal server error' });
    }
  }

  async consulta(req, res) {
    try {
      const tags = await Tags.find({});
      if (tags) {
        await tags;
      }
      return res.send();
    } catch (err) {
      return res.status(400).json({ error: 'Tags not found' });
    }
  }

  async atualiza(req, res) {
    try {
      const tags = await Tags.findById(req.body.id);
      if (tags) {
        await tags.updateOne();
      }
      return res.send();
    } catch (err) {
      return res.status(400).json({ error: 'Tags not found' });
    }
  }

  async remove(req, res) {
    try {
      const tags = await Tags.findById(req.body.id);
      if (tags) {
        await tags.remove();
      }
      return res.send();
    } catch (err) {
      return res.status(400).json({ error: 'Tags not found' });
    }
  }

}

module.exports = new TagsController();
