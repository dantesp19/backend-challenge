const Tasklist = require('../models/Tasklist');
const { v4: uuidv4 } = require('uuid');

class TasklistController {
  
  async grava(req, res) {
    try {
      const tasklist = new Tasklist({
        id: uuidv4(),
        name: req.body.name,
      });
      await tasklist.save();
      return res.json(task);
    } catch (err) {
      return res.status(500).json({ error: 'Internal server error' });
    }
  }

  async consulta(req, res) {
    try {
      const tasklist = await Tasklist.find({});
      if (tasklist) {
        await tasklist;
      }
      return res.send();
    } catch (err) {
      return res.status(400).json({ error: 'Task not found' });
    }

  }

  async atualiza(req, res) {
    try {
      const tasklist = await Tasklist.findById(req.body.id);
      if (tasklist) {
        await tasklist.updateOne();
      }
      return res.send();
    } catch (err) {
      return res.status(400).json({ error: 'Task not found' });
    }

  }

  async remove(req, res) {
    try {
      const tasklist = await Tasklist.findById(req.body.id);
      if (tasklist) {
        await tasklist.remove();
      }
      return res.send();
    } catch (err) {
      return res.status(400).json({ error: 'Task not found' });
    }
  }

}

module.exports = new TasklistController();
