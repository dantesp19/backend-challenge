const supertest = require('supertest');
const app = require('../../src/app');
const TaskModel = require('../../src/app/models/Tasklist');
const request = supertest(app);

describe('Task', () => {

  it('should be able to create task', async () => {

    const response = await request.post('/tasks').send({
      id: '814ee13a-0378-42b1-8256-e62b37972489',
      name: 'teste',
      notes: 'teste',
      priority: 'teste',
      remindMeOn: '2020-09-15T15:13:56.714',
      activityType: 'indoors',
      status: 'done',
      tasklist: '02c0cb76-5dfb-49c0-8ba8-2cc0eb14941c',
      tags: [
        {
          id: 'dddc9069-a388-404d-8432-cc3d7a1747e6',
          name: 'testea',
          count: 1,
        },
        {
          id: '09834dc1-00fb-4047-b9f7-d7f0fb8cad2c',
          name: 'testeb',
          count: 2,
        },
        {
          id: '6a02df63-e6dd-45e8-a3fa-30bd6782f572',
          name: 'testec',
          count: 3,
        }
      ],
    });

    expect(response.status);

  });

  it('should be able to get task', async () => {

    await TaskModel.find({});

    const response = await request.get('/task');

    expect(response.status);

  });

  it('should be able to update task', async () => {

    const task = new TaskModel({
      id: 'dddc9069-a388-404d-8432-cc3d7a1747e6',
      name: 'teste',
      notes: 'teste',
      priority: 'teste',
      remindMeOn: '2020-09-15T15:13:56.714Z',
      activityType: 'outdoor',
      status: 'done',
      tasklist: '02c0cb76-5dfb-49c0-8ba8-2cc0eb14941c',
      tags: [
        {
          id: 'dddc9069-a388-404d-8432-cc3d7a1747e6',
          name: 'testea',
          count: 1,
        },
        {
          id: '09834dc1-00fb-4047-b9f7-d7f0fb8cad2c',
          name: 'testeb',
          count: 2,
        },
        {
          id: 'bddbc306-a48a-4cb8-85f1-74cafeec0f19',
          name: 'tested',
          count: 3,
        }
      ],
    });

    await task.updateOne();

    const response = await request.patch('/tasks:id').send({
      id: task._id,
    });

    expect(response.status);

  });

  it('should be able to delete task', async () => {

    const task = new TaskModel({
      id: 'dddc9069-a388-404d-8432-cc3d7a1747e6',
      name: 'teste',
      notes: 'teste',
      priority: 'teste',
      remindMeOn: '2020-09-15T15:13:56.714Z',
      activityType: 'indoors',
      status: 'done',
      tasklist: '02c0cb76-5dfb-49c0-8ba8-2cc0eb14941c',
      tags: [
        {
          id: 'dddc9069-a388-404d-8432-cc3d7a1747e6',
          name: 'testea',
          count: 1,
        },
        {
          id: '09834dc1-00fb-4047-b9f7-d7f0fb8cad2c',
          name: 'testeb',
          count: 2,
        },
        {
          id: '6a02df63-e6dd-45e8-a3fa-30bd6782f572',
          name: 'testec',
          count: 3,
        }
      ],
    });

    await task.remove();

    const response = await request.delete('/tasks:id').send({
      id: task._id,
    });

    expect(response.status);

  });

});
