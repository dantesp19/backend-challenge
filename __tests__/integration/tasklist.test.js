const supertest = require('supertest');
const app = require('../../src/app');
const TasklistModel = require('../../src/app/models/Tasklist');
const request = supertest(app);

describe('Tasklist', () => {

  it('should be able to create tasklist', async () => {

    const response = await request.post('/tasklist').send({
      id: '02c0cb76-5dfb-49c0-8ba8-2cc0eb14941c',
      name: 'teste',
    });

    expect(response.status);

  });

  it('should be able to get tasklist', async () => {

    await TasklistModel.find({});

    const response = await request.get('/tasklist');

    expect(response.status);

  });

  it('should be able to update tasklist', async () => {

    const tasklist = new TasklistModel({
      id: '02c0cb76-5dfb-49c0-8ba8-2cc0eb14941c',
      name: 'teste',
    });

    await tasklist.updateOne();

    const response = await request.patch('/tasklist:id').send({
      id: tasklist._id,
    });

    expect(response.status);

  });

  it('should be able to delete tasklist', async () => {

    const tasklist = new TasklistModel({
      id: '02c0cb76-5dfb-49c0-8ba8-2cc0eb14941c',
      name: 'teste',
    });

    await tasklist.remove();

    const response = await request.delete('/tasklist:id').send({
      id: tasklist._id,
    });

    expect(response.status);

  });

});
