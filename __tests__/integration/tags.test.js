const supertest = require('supertest');
const app = require('../../src/app');
const TagsModel = require('../../src/app/models/Tags');
const request = supertest(app);

describe('Tags', () => {

  it('should be able to create tags', async () => {

    const response = await request.post('/tags').send({
      id: 'dddc9069-a388-404d-8432-cc3d7a1747e6',
      name: 'teste',
      count: 1,
    });

    expect(response.status);

  });

  it('should be able to get tags', async () => {

    await TagsModel.find({});

    const response = await request.get('/tags');

    expect(response.status);

  });

  it('should be able to update tags', async () => {

    const tags = new TagsModel({
      id: 'dddc9069-a388-404d-8432-cc3d7a1747e6',
      name: 'teste',
      count: 1,
    });

    await tags.updateOne();

    const response = await request.patch('/tags:id').send({
      id: tags._id,
    });

    expect(response.status);

  });

  it('should be able to delete tags', async () => {

    const tags = new TagsModel({
      id: 'dddc9069-a388-404d-8432-cc3d7a1747e6',
      name: 'teste',
      count: 1,
    });

    await tags.remove();

    const response = await request.delete('/tags:id').send({
      id: tags._id,
    });

    expect(response.status);

  });

});
